# -*- coding=utf-8 -*-

import os


COMMANDS = \
'''
npm install --save webpack webpack-dev-middleware webpack-hot-middleware
npm install --save react react-dom react-redux
npm install --save redux redux-logger redux-thunk
npm install --save express
npm install --save express-partials
npm install --save classnames

npm install --save-dev babel-core babel-loader babel-preset-es2015 babel-preset-react babel-preset-react-hmre
npm install --save-dev babel-polyfill
npm install --save-dev babel-plugin-react-transform
npm install --save-dev expect
npm install --save-dev jsdom
npm install --save-dev mocha
npm install --save-dev node-libs-browser
npm install --save-dev raw-loader
npm install --save-dev react-addons-test-utils
npm install --save-dev react-transform-hmr
npm install --save-dev redux-devtools
npm install --save-dev redux-devtools-chart-monitor
npm install --save-dev redux-devtools-dock-monitor
npm install --save-dev style-loader
npm install --save-dev css-loader
npm install --save-dev less
npm install --save-dev less-loader
'''

COMMAND_LIST = [s for s in [x.strip() for x in COMMANDS.split("\n")] if s]

for command in COMMAND_LIST:
    print(command)
    os.system(command)
    print()
