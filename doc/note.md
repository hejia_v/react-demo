# 学习笔记

## 全局安装一些必要的库
```
npm install -g express
npm install -g express-generator
npm install -g supervisor
npm install -g nodemon
```

## 创建git仓库
git init


## 初始化npm

* 创建Express项目
    建立工程结构
    ```
    express -e ejs notebook-03
    ```
    -e ejs, -j jade

* 初始化npm并生成package.json  
    如果先初始化git仓库，在执行这步，package.json会有git相关的配置  
    npm init -y

## 添加npm模块
npm install --save webpack webpack-dev-middleware webpack-hot-middleware
npm install --save react react-dom react-redux
npm install --save redux redux-logger redux-thunk
npm install --save-dev babel-core babel-loader babel-preset-es2015 babel-preset-react babel-preset-react-hmre
npm install --save express
npm install --save express-partials
npm install --save classnames

npm install --save-dev babel-plugin-react-transform
npm install --save-dev babel-polyfill
npm install --save-dev expect
npm install --save-dev jsdom
npm install --save-dev mocha
npm install --save-dev node-libs-browser
npm install --save-dev raw-loader
npm install --save-dev react-addons-test-utils
npm install --save-dev react-transform-hmr
npm install --save-dev redux-devtools
npm install --save-dev redux-devtools-chart-monitor
npm install --save-dev redux-devtools-dock-monitor
npm install --save-dev style-loader

## 自动重启项目工程

* 浏览器访问
    http://127.0.0.1:3000/
    http://localhost:3000/

* 直接启动
    ```
    npm start
    ```

* 使用 supervisor 命令启动 app.js
    ```
    supervisor ./bin/www
    ```
    当代码被改动时，运行的脚本会被终止，然后重新启动。

* nodemon
    ```
    nodemon ./bin/www
    ```

## 查看本机支持ES6的程度
npm install -g es-checker
es-checker

## 调试
node-inspector
