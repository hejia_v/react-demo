var express = require('express');
var router = express.Router();

// 该路由使用的中间件
router.use(function timeLog(req, res, next) {
  console.log('Time: ', Date.now());
  next();
});

router.get('/', function (req, res, next) {
    console.log('response will be sent by the next function ...');
    next();
}, function (req, res) {
    res.render('index', {
        title: 'Express',
        layout: 'layout-main.ejs'
    });
});

router.get('/test', function (req, res) {
    res.render('test', {
        title: 'Express',
    });
});

// 定义 about 页面的路由
router.get('/about', function(req, res) {
  res.send('About birds');
});

module.exports = router;
