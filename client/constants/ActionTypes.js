export const ADD_TODO = 'ADD_TODO'
export const DELETE_TODO = 'DELETE_TODO'
export const EDIT_TODO = 'EDIT_TODO'
export const COMPLETE_TODO = 'COMPLETE_TODO'
export const COMPLETE_ALL = 'COMPLETE_ALL'
export const CLEAR_COMPLETED = 'CLEAR_COMPLETED'

// 参考网页
export const ADD_REF_SITE = 'ADD_REF_SITE'
export const DEL_REF_SITE = 'DEL_REF_SITE'
export const EDIT_REF_SITE = 'EDIT_REF_SITE'

// 网址组
export const ADD_SITE_GROUP = 'ADD_SITE_GROUP'
export const DEL_SITE_GROUP = 'DEL_SITE_GROUP'
export const EDIT_SITE_GROUP = 'EDIT_SITE_GROUP'

//modal
export const OPEN_MODAL = 'OPEN_MODAL'
export const CLOSE_MODAL = 'CLOSE_MODAL'
