import { OPEN_MODAL, CLOSE_MODAL } from '../constants/ActionTypes'
import { MODAL_SITE_GROUP_NEW } from '../constants/ModalTypes'

const initialState = {
    isModalOpen: false,
    modalType: MODAL_SITE_GROUP_NEW,
    data: {},
    callback: undefined
}


export default function modal(state = initialState, action) {
    switch (action.type) {
        case OPEN_MODAL:
            return Object.assign({}, state, {
                isModalOpen: true,
                modalType: action.modalType,
                data: action.data,
                callback: action.callback
             })

        case CLOSE_MODAL:
            return Object.assign({}, state, {
                isModalOpen: false,
             })

        default:
            return state
    }
}
