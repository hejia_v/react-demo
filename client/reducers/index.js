import { combineReducers } from 'redux'
import todos from './todos'
import site_groups from './site_groups'
import modal from './modal'

const rootReducer = combineReducers({
    // todos,
    site_groups,
    modal
})

export default rootReducer
