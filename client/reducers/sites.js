import { ADD_REF_SITE, DEL_REF_SITE, EDIT_REF_SITE } from '../constants/ActionTypes'

const initialState = [{
    id: 0,
    name: "aaaaaaaa",
    date: "04 02",
    sites: [{
        id: 0,
        text: 'http://keenwon.com/1524.html'
    },{
        id: 1,
        text: 'http://keenwon.com/1524.html'
    },{
        id: 2,
        text: 'http://keenwon.com/1524.html'
    },{
        id: 3,
        text: 'http://keenwon.com/1524.html'
    }]
},{
    id: 1,
    name: "aaaaaaaa",
    date: "04 02",
    sites: [{
        id: 0,
        text: 'http://keenwon.com/1524.html'
    },{
        id: 1,
        text: 'http://keenwon.com/1524.html'
    },{
        id: 2,
        text: 'http://keenwon.com/1524.html'
    },{
        id: 3,
        text: 'http://keenwon.com/1524.html'
    }]
},{
    id: 2,
    name: "aaaaaaaa",
    date: "04 02",
    sites: [{
        id: 0,
        text: 'http://keenwon.com/1524.html'
    },{
        id: 1,
        text: 'http://keenwon.com/1524.html'
    },{
        id: 2,
        text: 'http://keenwon.com/1524.html'
    },{
        id: 3,
        text: 'http://keenwon.com/1524.html'
    }]
}]


export default function sites(state = initialState, action) {
    switch (action.type) {
        case ADD_REF_SITE:
            return [
                ...state,
                {
                    id: state.reduce((maxId, site) => Math.max(site.id,
                        maxId), -1) + 1,
                    text: action.text
                }
            ]

        case DEL_REF_SITE:
            return state.filter(site => site.id !== action.id)

        case EDIT_REF_SITE:
            return state.map(site =>
                site.id === action.id ?
                Object.assign({}, site, { text: action.text }) :
                site
            )

        default:
            return state
    }
}
