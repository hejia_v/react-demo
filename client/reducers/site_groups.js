import { ADD_REF_SITE, DEL_REF_SITE, EDIT_REF_SITE, ADD_SITE_GROUP, DEL_SITE_GROUP,
    EDIT_SITE_GROUP } from '../constants/ActionTypes'

const initialState = [{
    id: 0,
    name: "aaaaaaaa",
    date: "04 02",
    sites: [{
        id: 0,
        text: 'http://keenwon.com/1524.html'
    },{
        id: 1,
        text: 'http://keenwon.com/1524.html'
    },{
        id: 2,
        text: 'http://keenwon.com/1524.html'
    },{
        id: 3,
        text: 'http://keenwon.com/1524.html'
    }]
},{
    id: 1,
    name: "aaaaaaaa",
    date: "04 02",
    sites: [{
        id: 0,
        text: 'http://keenwon.com/1524.html'
    },{
        id: 1,
        text: 'http://keenwon.com/1524.html'
    },{
        id: 2,
        text: 'http://keenwon.com/1524.html'
    },{
        id: 3,
        text: 'http://keenwon.com/1524.html'
    }]
},{
    id: 2,
    name: "aaaaaaaa",
    date: "04 02",
    sites: [{
        id: 0,
        text: 'http://keenwon.com/1524.html'
    },{
        id: 1,
        text: 'http://keenwon.com/1524.html'
    },{
        id: 2,
        text: 'http://keenwon.com/1524.html'
    },{
        id: 3,
        text: 'http://keenwon.com/1524.html'
    }]
}]


const sites = (state, action) => {
    switch (action.type) {
        case ADD_REF_SITE:
            return [
                ...state,
                {
                    id: state.reduce((maxId, site) => Math.max(site.id,
                        maxId), -1) + 1,
                    text: action.text
                }
            ]

        case DEL_REF_SITE:
            return state.filter(site => site.id !== action.id)

        case EDIT_REF_SITE:
            return state.map(site =>
                site.id === action.id ?
                Object.assign({}, site, { text: action.text }) :
                site
            )

        default:
            return state
    }
}


const groups = (state = initialState, action) => {
    switch (action.type) {
        case ADD_SITE_GROUP:
            return [
                ...state,
                {
                    id: state.reduce((maxId, group) => Math.max(group.id,
                        maxId), -1) + 1,
                    name: action.name,
                    date: action.date,
                    sites: []
                }
            ]

        case DEL_SITE_GROUP:
            return state.filter(group => group.id !== action.id)

        case EDIT_SITE_GROUP:
            return state.map(group =>
                group.id === action.id ?
                Object.assign({}, group, { name: action.name, date: action.date }) :
                group
            )

        case ADD_REF_SITE:
            return state.map(group =>
                group.id === action.gid ?
                Object.assign({}, group, { sites: sites(group.sites, action) }) :
                group
            )

        case DEL_REF_SITE:
            return state.map(group =>
                group.id === action.gid ?
                Object.assign({}, group, { sites: sites(group.sites, action) }) :
                group
            )

        case EDIT_REF_SITE:
            return state.map(group =>
                group.id === action.gid ?
                Object.assign({}, group, { sites: sites(group.sites, action) }) :
                group
            )

        default:
            return state
    }
}

export default groups
