import { createStore, compose, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import rootReducer from '../reducers'
// import DevTools from '../containers/DevTools';


// compose 复合函数 (f · g)(x) = f(g(x))
//applyMiddleware来自redux, 可以包装 store 的 dispatch
//thunk作用是使被 dispatch 的 function 会接收 dispatch 作为参数，并且可以异步调用它


const createStoreWithMiddleware = (reducer, initialState) => createStore(
    reducer,
    initialState,
    compose(
        applyMiddleware(thunk, logger()),
        window.devToolsExtension ? window.devToolsExtension() : f => f
        // applyMiddleware(thunk),
        // DevTools.instrument()
    )
);


export default function configureStore(initialState) {
    const store = createStoreWithMiddleware(rootReducer, initialState);

    //热替换选项
    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('../reducers', () => {
            const nextReducer = require('../reducers')
            store.replaceReducer(nextReducer)
        })
    }
    return store;
}
