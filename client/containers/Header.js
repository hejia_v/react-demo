import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import classnames from 'classnames'
import * as SiteActions from '../actions/sites.js'

class Header extends Component {
    constructor(props, context) {
        super(props, context)
        // this.state = { isModalOpen: false }
    }

    render() {
        const { groups, actions } = this.props

        {/*<SiteGroupHeader />
        <SiteGroupsMain groups={groups}
        onAddGroup={this.handleOpenDlg.bind(this)}
        actions={actions} />
        <Dialog isModalOpen={this.state.isModalOpen}
        onCloseDlg={this.handleCloseDlg.bind(this)} />*/}
        return (
            <header className="row">
                <div id="logo_text">
                    <h1><a href="index.html">秋风冷画屏</a></h1>
                    <h2>桃李春风一杯酒，江湖夜雨十年灯</h2>
                </div>

                <div className="container">
                    <ul className="nav nav-pills">
                        <li role="presentation" className="active"><a href="#">首页</a></li>
                        <li role="presentation"><a href="#">文章</a></li>
                        <li role="presentation"><a href="#">收藏</a></li>
                        <li role="presentation"><a href="#">小说</a></li>
                        <li role="presentation"><a href="#">诗词</a></li>
                        <li role="presentation"><a href="#">积累</a></li>
                        <li role="presentation"><a href="#">灵感</a></li>
                        <li role="presentation"><a href="#">掠影</a></li>
                        <li role="presentation"><a href="#">链接</a></li>
                    </ul>

                    <form className="navbar-form navbar-left" role="search">
                        <div className="form-group">
                            <input type="text" className="form-control" placeholder="搜索"/>
                        </div>
                        <button type="submit" className="btn btn-default">搜索</button>
                    </form>
                </div>
            </header>
        )
    }
}

Header.propTypes = {
    groups: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired
}

function mapStateToProps(state) {
    return {
        groups: state.site_groups
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(SiteActions, dispatch)
    }
}

export default connect( mapStateToProps, mapDispatchToProps )(Header);
