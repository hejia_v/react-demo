import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import SiteGroupHeader from '../components/SiteGroupHeader'
import SiteGroupsMain from '../components/SiteGroupsMain'
import * as SiteActions from '../actions/sites.js'
import * as ModalActions from '../actions/modal.js'

class SiteGroups extends Component {
    constructor(props, context) {
        super(props, context)
    }

    render() {
        const { groups, siteActions, modalActions } = this.props

        return (
            <div>
                <SiteGroupHeader />
                <SiteGroupsMain groups={groups}
                    siteActions={siteActions}
                    modalActions={modalActions} />
            </div>
        )
    }
}

SiteGroups.propTypes = {
    groups: PropTypes.array.isRequired,
    siteActions: PropTypes.object.isRequired,
    modalActions: PropTypes.object.isRequired
}

function mapStateToProps(state) {
    return {
        groups: state.site_groups
    }
}

function mapDispatchToProps(dispatch) {
    return {
        siteActions: bindActionCreators(SiteActions, dispatch),
        modalActions: bindActionCreators(ModalActions, dispatch)
    }
}

export default connect( mapStateToProps, mapDispatchToProps )(SiteGroups);
