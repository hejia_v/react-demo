import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import classnames from 'classnames'
import * as ModalActions from '../actions/modal.js'


class Modal extends Component {
    constructor(props, context) {
        super(props, context)
        // this.state = { isOpen: false }
    }

    handleEditActive() {
        this.setState({ editing: true })
    }

    render() {
        const { modal, actions } = this.props
        const isModalOpen = modal.isModalOpen

        console.log(modal);

        return (
            <div className={classnames(
                    "modal-overlay", {
                        opening: isModalOpen,
                        closing: !isModalOpen
                    })}>
            </div>
        )
    }
}

Modal.propTypes = {
    modal: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired
}

function mapStateToProps(state) {
    return {
        modal: state.modal
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(ModalActions, dispatch)
    }
}

export default connect( mapStateToProps, mapDispatchToProps )(Modal);
