// import 'babel-core/polyfill'
import 'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import Header from './containers/Header'
import SiteGroups from './containers/SiteGroups'
import Modal from './containers/Modal'
import configureStore from './store/configureStore'
import './stylesheets/sites.less'
import './stylesheets/index.less'
import './stylesheets/modal.less'


const store = configureStore()

// Provider可以看做一个上下文，隐式的向下传递store
render(
    <Provider store={store}>
        <div>
            <Header />
            <section className="main">
                <SiteGroups />
            </section>
            <Modal />
        </div>
    </Provider>,
    document.getElementById('app')
)
