import React, { Component, PropTypes } from 'react'
import SiteGroupItem from './SiteGroupItem'
import { MODAL_SITE_GROUP_NEW } from '../constants/ModalTypes'


class SiteGroupsMain extends Component {
    constructor(props, context) {
        super(props, context)
    }

    handleAddGroup() {
        console.log(111111111111111);
        const data = {}
        this.props.modalActions.openModal(MODAL_SITE_GROUP_NEW, data,
            this.addGroupCallback)
        // return eModal
        //     .prompt({ size: eModal.size.sm, message: as, title })
        //     // .prompt({ size: eModal.size.sm, message: '组名', title })
        //     .then();
    }

    addGroupCallback(data) {
        console.log("addGroupCallback -----");
    }

    render() {
        const { groups, siteActions } = this.props

        return (
            <div className="panel-group" id="accordion">
                {groups.map(group =>
                    <SiteGroupItem key={group.id} group={group} actions={siteActions} />
                    )}

                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h4 className="panel-title">
                            <a className="add-site-group"
                                onClick={this.handleAddGroup.bind(this)}>添加组</a>
                        </h4>
                    </div>
                </div>
            </div>
        )
    }
}

SiteGroupsMain.propTypes = {
    // onAddGroup: PropTypes.func.isRequired,
}

export default SiteGroupsMain
