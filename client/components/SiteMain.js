import React, { Component, PropTypes } from 'react'
import SiteItem from './SiteItem'
import SiteFooter from './SiteFooter'

class SiteMain extends Component {
    constructor(props, context) {
        super(props, context)
    }

    render() {
        const { gid, sites, actions } = this.props

        return (
            <section className="sites-main">
                <ul>
                    {sites.map(site =>
                        <SiteItem key={site.id} gid={gid} site={site} {...actions} />
                    )}
                </ul>
                <SiteFooter gid={gid} {...actions}/>
            </section>
        )
    }
}

export default SiteMain
