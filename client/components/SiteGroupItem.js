import React, { Component, PropTypes } from 'react'
import classnames from 'classnames'
import SiteMain from '../components/SiteMain'

class SiteGroupItem extends Component {
    constructor(props, context) {
        super(props, context)
    }

    render() {
        const { group, actions } = this.props
        const sites = group.sites || []
        const count = sites.length
        const sPanelId = "collapse" + group.id

        return (
            <div className="panel panel-default site-group">
                <div className="panel-heading">
                    <h4 className="panel-title">
                    <a data-toggle="collapse" href={"#" + sPanelId}>
                        {group.name + " " + group.date}
                        <span className="badge">{count}</span></a>
                    </h4>
                </div>
                <div id={sPanelId} className="panel-collapse collapse in">
                    <div className="panel-body">
                        <SiteMain gid={group.id} sites={sites} actions={actions} />
                    </div>
                </div>
            </div>
        )
    }
}

SiteGroupItem.propTypes = {
    group: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired
}

export default SiteGroupItem
