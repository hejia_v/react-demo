import React, { Component, PropTypes } from 'react'
import classnames from 'classnames'
// import RefSiteInput from './RefSiteInput'

class SiteFooter extends Component {
    constructor(props, context) {
        super(props, context)
        this.state = {
            text: this.props.text || ''
        }
    }

    handleSubmit(e) {
        const text = this.state.text.trim()
        if (text.length != 0) {
            this.props.addRefSite(text, this.props.gid)
            this.setState({ text: '' })
        }
    }

    handleChange(e) {
        this.setState({
            text: e.target.value
        })
    }

    render() {
        return (
            <footer>
                <div className="form-group">
                    <input className="form-control"
                        id="inputName"
                        placeholder="网址"
                        required=""
                        type="text"
                        value = { this.state.text }
                        onChange = { this.handleChange.bind(this) }
                    />
                </div>
                <button className="btn btn-primary"
                    onClick = { this.handleSubmit.bind(this) }>添加</button>
            </footer>
        )
    }
}

SiteFooter.propTypes = {
    gid: PropTypes.number.isRequired,
    text: PropTypes.string,
    addRefSite: PropTypes.func.isRequired,
}

export default SiteFooter
