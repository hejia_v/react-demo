import React, { Component, PropTypes } from 'react'
import classnames from 'classnames'

class SiteGroupHeader extends Component {
    constructor(props, context) {
        super(props, context)
        this.state = { index: 0 }
    }

    handleClick(index) {
        if (index != this.state.index) {
            this.setState({ index: index })
        }
    }

    render() {
        const index = this.state.index

        return (
            <ul className="nav nav-tabs">
                <li role="presentation" className={classnames({active: index === 0})}
                    onClick={(text) => this.handleClick(0)}>
                    <a href="#">全部</a></li>
                <li role="presentation" className={classnames({active: index === 1})}
                    onClick={(text) => this.handleClick(1)}>
                    <a href="#">分组</a></li>
            </ul>
        )
    }
}

SiteGroupHeader.propTypes = {
    // group: PropTypes.object.isRequired,
    // editRefSite: PropTypes.func.isRequired,
}

export default SiteGroupHeader
