import * as types from '../constants/ActionTypes'

export function openModal(type, data, callback) {
    return { type: types.OPEN_MODAL, modalType: type, data, callback }
}

export function closeModal() {
    return { type: types.CLOSE_MODAL}
}
