import * as types from '../constants/ActionTypes'

export function addRefSite(text, gid) {
    return { type: types.ADD_REF_SITE, text, gid }
}

export function deleteRefSite(id, gid) {
    return { type: types.DEL_REF_SITE, id, gid }
}

export function editRefSite(id, text, gid) {
    return { type: types.EDIT_REF_SITE, id, text, gid }
}

// 网址组
export function addSiteGroup(name, date) {
    return { type: types.ADD_SITE_GROUP, name, date }
}

export function delSiteGroup(id) {
    return { type: types.DEL_SITE_GROUP, id}
}

export function editSiteGroup(id, name, date) {
    return { type: types.EDIT_SITE_GROUP, id, name, date }
}
